// Package bengalspi provides access to the SPI bus on a Versalogic Bengal
// CPU board. The SPI bus on this board is implemented by an FPGA mapped
// into the ISA-bus I/O space.
package bengalspi

import (
	"context"
	"errors"
	"io"
	"log"
	"os"
	"sync"
	"time"
)

const (
	IsaOffset int64 = 0xc80
	CtlReg          = 0x08
	StatusReg       = 0x09
	DataReg         = 0x0a
	MiscReg         = 0x0e
)

type ClkPolarity int

const (
	Low  ClkPolarity = 0
	High             = 1
)

type ClkPhase int

const (
	RisingEdge  ClkPhase = 0
	FallingEdge          = 1
)

type FrameSize int

const (
	Spi8bit  FrameSize = 0
	Spi16bit           = 1
	Spi24bit           = 2
	Spi32bit           = 3
)

type ClockFreq int

const (
	Freq1Mhz ClockFreq = 0
	Freq2Mhz           = 1
	Freq4Mhz           = 2
	Freq8Mhz           = 3
)

type ShiftDir int

const (
	MsbFirst ShiftDir = 0
	LsbFirst          = 1
)

// SPI loopback enable
const SpiLoopback uint8 = 2

type Config struct {
	// Clock polarity
	Polarity ClkPolarity
	// Clock phase
	Phase ClkPhase
	// Transfer size
	Size FrameSize
	// Slave chip select
	SlaveSel int
	// Bus clock frequency
	Clock ClockFreq
	// Bit transfer order
	Order ShiftDir
}

// ControlValue returns the value to load into the SPI control register
func (c Config) ControlValue() uint8 {
	return uint8(c.SlaveSel) |
		(uint8(c.Size) << 4) |
		(uint8(c.Phase) << 6) |
		(uint8(c.Polarity) << 7)
}

// StatusValue returns the value to load into the SPI status register
func (c Config) StatusValue() uint8 {
	return (uint8(c.Order) << 2) | (uint8(c.Clock) << 4)
}

// BytesPerFrame returns the number of bytes transferred in each SPI
// bus transaction.
func (c Config) BytesPerFrame() int {
	return int(c.Size) + 1
}

type ReadWriterAt interface {
	io.ReaderAt
	io.WriterAt
}

var BadXferSize = errors.New("Bad data transfer size")

// Message contains the bytes sent to and returned from the Slave device.
type Message struct {
	Out []byte
	In  []byte
}

type Bus struct {
	fp     ReadWriterAt
	offset int64
	mu     *sync.Mutex
	debug  bool
	cfg    Config
}

// Make it easier for the compiler to catch a value/mask argument
// swap when calling modifyReg
type bits uint8

func (s *Bus) modifyReg(reg int64, value uint8, mask bits) (uint8, error) {
	var b [1]byte

	s.mu.Lock()
	defer s.mu.Unlock()

	offset := s.offset + reg
	_, err := s.fp.ReadAt(b[:], offset)
	if err != nil {
		return 0, err
	}

	if s.debug {
		log.Printf("Read @%#x: %#02x", offset, b[0])
	}

	b[0] = (b[0] &^ uint8(mask)) | (value & uint8(mask))
	_, err = s.fp.WriteAt(b[:], offset)

	if s.debug {
		log.Printf("Write @%#x: %#02x", offset, b[0])
	}

	return b[0], err
}

func (s *Bus) writeRegs(start int64, vals []uint8) error {
	s.mu.Lock()
	defer s.mu.Unlock()
	_, err := s.fp.WriteAt(vals, s.offset+start)
	if s.debug {
		log.Printf("Write %d @%#x: %#02x", len(vals), start, vals)
	}
	return err
}

func (s *Bus) readRegs(start int64, vals []uint8) error {
	s.mu.Lock()
	defer s.mu.Unlock()
	_, err := s.fp.ReadAt(vals, s.offset+start)
	if s.debug {
		log.Printf("Read %d @%#x: %#02x", len(vals), start, vals)
	}
	return err
}

func (s *Bus) busyWait(ctx context.Context) error {
	var b [1]byte
	offset := s.offset + StatusReg
loop:
	for {
		select {
		case <-ctx.Done():
			return ctx.Err()
		default:
			_, err := s.fp.ReadAt(b[:], offset)
			if err != nil {
				return err
			}
			if (b[0] & 0x01) == 0 {
				break loop
			}
		}
	}
	return nil
}

// NewBus returns a new Bus instance at the IO address specified by offset. The
// implementation uses /dev/port to access the IO space and requires root
// privileges.
func NewBus(offset int64, debug bool) *Bus {
	f, err := os.OpenFile("/dev/port", os.O_RDWR|os.O_SYNC, 0660)
	if err != nil {
		return nil
	}
	return &Bus{
		fp:     f,
		offset: offset,
		mu:     &sync.Mutex{},
		debug:  debug,
	}
}

// SetDebug enables or disables logging output.
func (s *Bus) SetDebug(state bool) {
	s.debug = state
}

// Enable or disable SPI loopback mode
func (s *Bus) SetLoopback(state bool) error {
	var err error
	if state {
		s.modifyReg(MiscReg, SpiLoopback, bits(SpiLoopback))
	} else {
		s.modifyReg(MiscReg, 0, bits(SpiLoopback))
	}
	return err
}

// Setup sets the SPI transfer parameters
func (s *Bus) Setup(cfg Config) error {
	v := cfg.ControlValue()
	_, err := s.modifyReg(CtlReg, v, bits(v))
	if err != nil {
		return err
	}

	v = cfg.StatusValue()
	_, err = s.modifyReg(StatusReg, v, bits(v))
	if err != nil {
		return err
	}
	s.cfg = cfg

	return nil
}

func (s *Bus) writeData(ctx context.Context, idx int64, p []byte) error {
	err := s.writeRegs(DataReg+idx, p)
	if err != nil {
		return err
	}
	ctx, cancel := context.WithTimeout(ctx, 100*time.Millisecond)
	defer cancel()
	return s.busyWait(ctx)
}

// DoXfer performs one or more SPI bus transfers (transactions) to send
// the contents of m.Out to the slave device. It returns the total number
// of transfers along with any error that occurs. Upon return, m.In will
// contain the response (if any) from the slave device.
func (s *Bus) DoXfer(m *Message) (int, error) {
	n_in, n_out := len(m.In), len(m.Out)
	// For simplicity, ensure both buffers are the same size.
	switch {
	case n_in > n_out:
		m.Out = append(m.Out, make([]byte, n_in-n_out)...)
	case n_out > n_in:
		m.In = append(m.In, make([]byte, n_out-n_in)...)
	}
	n_in = n_out

	// Ensure we have an integral number of frames
	inc := s.cfg.BytesPerFrame()
	if (n_out % inc) != 0 {
		return 0, BadXferSize
	}

	// Index of the starting data register (0-3). Writing to data
	// register 3 triggers the SPI transaction and starts clocking
	// out the frame data.
	idx := int64(4 - inc)

	ctx := context.Background()
	var (
		err   error
		count int
	)

	// Step through the slice of output bytes one frame at a time
	for i := 0; i < n_out; i += inc {
		err = s.writeData(ctx, idx, m.Out[i:i+inc])
		if err != nil {
			return count, err
		}

		err = s.readRegs(DataReg+idx, m.In[i:i+inc])
		if err != nil {
			return count, err
		}

		count++
	}

	return count, nil
}
