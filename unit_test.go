package bengalspi

import (
	"sync"
	"testing"
)

type rwbuffer struct {
	buf     [64]byte
	verbose bool
}

func (rw *rwbuffer) ReadAt(p []byte, offset int64) (n int, err error) {
	n = len(p)

	for i := 0; i < n; i++ {
		p[i] = rw.buf[offset+int64(i)]
	}

	return
}

func (rw *rwbuffer) WriteAt(p []byte, offset int64) (n int, err error) {
	n = len(p)
	for i := 0; i < n; i++ {
		rw.buf[offset+int64(i)] = p[i]
	}

	return
}

func (rw *rwbuffer) GetByte(i int) byte {
	return rw.buf[i]
}

func TestSetup(t *testing.T) {
	table := []struct {
		cfg       Config
		ctl, stat uint8
	}{
		{
			cfg:  Config{},
			ctl:  0,
			stat: 0,
		},
		{
			cfg: Config{Polarity: Low, Phase: RisingEdge, Size: Spi16bit,
				SlaveSel: 1, Clock: Freq4Mhz},
			ctl:  0x11,
			stat: 0x20,
		},
	}
	rw := &rwbuffer{}
	bus := &Bus{
		fp:     rw,
		offset: 0,
		mu:     &sync.Mutex{},
		debug:  testing.Verbose(),
	}

	for _, e := range table {
		bus.Setup(e.cfg)
		if rw.GetByte(CtlReg) != e.ctl {
			t.Errorf("Bad ctl reg value; expected %08b, got %08b",
				e.ctl, rw.GetByte(CtlReg))
		}
		if rw.GetByte(StatusReg) != e.stat {
			t.Errorf("Bad status reg value; expected %08b, got %08b",
				e.stat, rw.GetByte(StatusReg))
		}
	}
}

func TestXfer(t *testing.T) {
	rw := &rwbuffer{}
	bus := &Bus{
		fp:     rw,
		offset: 0,
		mu:     &sync.Mutex{},
		debug:  testing.Verbose(),
	}
	bus.Setup(Config{Size: Spi24bit})
	m := Message{
		Out: []byte{3, 2, 1},
	}

	n, err := bus.DoXfer(&m)
	if err != nil {
		t.Fatal(err)
	}

	if n != 1 {
		t.Errorf("Bad transfer count; expected 1, got %d", n)
	}

	if m.In == nil {
		t.Fatalf("No data received")
	}

	if string(m.In) != string(m.Out) {
		t.Errorf("Buffer mismatch: %q != %q", m.In, m.Out)
	}
}

func TestXferError(t *testing.T) {
	rw := &rwbuffer{}
	bus := &Bus{
		fp:     rw,
		offset: 0,
		mu:     &sync.Mutex{},
		debug:  testing.Verbose(),
	}
	bus.Setup(Config{Size: Spi24bit})
	m := Message{
		Out: []byte{4, 3, 2, 1},
	}

	_, err := bus.DoXfer(&m)
	if err == nil {
		t.Fatal("DoXfer error not detected")
	}

	if err != BadXferSize {
		t.Errorf("Wrong error type: %T", err)
	}
}
